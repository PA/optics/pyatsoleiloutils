# pyatsoleiloutils

This is a package developed to ease the interaction with pyat at SOLEIL.

It provides:
- custom plot synoptics for at
- harmonic rf flat potential condition setup
- saves momap in hdf5 file format
- adds frequency map functions for the SOLEIL II ring and booster
- adds plots for frequency maps
- adds opticsbeat function
- interface to SC rings
- functions to calculate lifetime and momentum aperture

To install:
1) git clone the project
2) cd to the folder
3) pip install .

To reinstall:
1) rm -rf build
2) pip install .

Requirements:
accelerator-toolbox from sources,
h5py,
numpy, scipy, matplotlib

orblancog
