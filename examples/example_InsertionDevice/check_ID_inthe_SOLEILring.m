% This file inserts an Insertion Device (ID) in a SOLEIL straight section,
%   and calculates the tune difference
% orblancog
% 2022jan19

THERING = lat_soleil;

% create element
idu20 = atidtable_dat('u20', 10, 'u20_g45mm_kicks_2022nov10.txt', 2.75, 'IdTablePass');

% choose one drift
list_sd=findcells(THERING,'FamName','SDAC1');
insertIDindex = list_sd(5);

% create NEWRING with the field in one straight section
NEWRING = THERING;
NEWRING{insertIDindex}.Length = NEWRING{insertIDindex}.Length + NEWRING{insertIDindex + 1}.Length;
NEWRING(insertIDindex+1) = [];
NEWRING = atinsertelems(NEWRING,insertIDindex,0.5,idu20);
insertIDindex = insertIDindex+1;

findorbit6(THERING);
findorbit6(NEWRING);

% calculate tune difference
[nu0, lin0] = atlinopt6(THERING);
[nu1, lin1] = atlinopt6(NEWRING);
% diff 0.0000    0.0044   -0.0000
nu1.tune - nu0.tune