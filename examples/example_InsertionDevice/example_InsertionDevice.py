### example: insert ID in the ring
# 2023feb05
# orblancog

print('\n\nExample to insert an Insertion Device in a ring')
print('    and get the tune variation\n\n')

import at
# load ID file
# u20 = at.InsertionDevice('u20',10,"u20_g45mm_kicks_2022nov10.txt", 2.75)
# 2023feb03 element name changes to IdTable
u20 = at.InsertionDeviceKickMap('u20',10,"u20_g45mm_kicks_2022nov10.txt", 2.75)

# load lattice
lattice = at.load_m('lat_soleil.m')
_, beamdata_lat, _    = at.get_optics(lattice,get_chrom=True)
print(f'tune  : {beamdata_lat.tune}')
print(f'orbit : {at.find_orbit(lattice)}')

# choose a drift and get the index
dr_name = 'SDAC1'
refDRIFT = at.get_cells(lattice, 'FamName', dr_name)
# get index of drift
dr_index = at.get_refpts(lattice,dr_name)
# choose next element downstream the desired location of the ID
insertIDindex = dr_index[5]

# create a new lattice where we will insert the InsertionDevice
newlattice = lattice.deepcopy()
### 2023feb05: insert function does not work, it removes the Energy property
### newlattice.insert(insertIDindex+1, u20)
### work around because at.lattice.insert does not copy the Energy property
dummyelem = newlattice[insertIDindex].deepcopy()
newlattice.insert(insertIDindex, dummyelem)
newlattice[insertIDindex] = u20.deepcopy()
# substract half length on each side
newlattice[insertIDindex - 1].Length = newlattice[insertIDindex - 1].Length - u20.Length/2;
newlattice[insertIDindex + 1].Length = newlattice[insertIDindex + 1].Length - u20.Length/2;

## choose pass method, for a quick test
#newlattice[insertIDindex].PassMethod = 'DriftPass'
#newlattice[insertIDindex].PassMethod = 'IdTablePass'
#newlattice[insertIDindex].set_DriftPass()
newlattice[insertIDindex].set_IdTablePass()
print(newlattice[insertIDindex].get_PassMethod())

print(f'orbit w ID: {at.find_orbit(newlattice)}')
[_,beamdata_newlat,_] = at.get_optics(newlattice, get_chrom=True)
print(f'tune  w ID: {beamdata_newlat.tune}')

## expected difference : 0.0000 0.0044 -0.0000 (from matlab)
print(f' ... Tune variation ...')
print(f'expected difference  : 0.0000 0.0044 -0.0000 (from matlab)')
print(f'calculated difference:{beamdata_newlat.tune - beamdata_lat.tune} (from python)')

print(u20)
