# this file calculates the effect of a B9 component in the 412 sextupoles.
# orblancog
# 2023jun13

import pyatsoleiloutils

# read the SC structure with the IDEALRING and the one with multipoles
idealring,ring=pyatsoleiloutils.outils4SC.loadSCmatfile('SCmult.mat')

# set 4D
idealring.disable_6d()
ring.disable_6d()

# calculate the map
pyatsoleiloutils.do_SOLEIL_II_fmap(idealring,nproc=64, fout='fmapIDEALRING.hdf5')
pyatsoleiloutils.do_SOLEIL_II_fmap(ring,nproc=64, fout='fmapRING.hdf5')

# plot the maps
pyatsoleiloutils.plot_SOLEIL_II_fmaphdf5(
        'fmapIDEALRING.hdf5',
        diffusionfn="diffusionIDEALRING.pdf",
        fmapfn='fmapIDEALRING.pdf')
pyatsoleiloutils.plot_SOLEIL_II_fmaphdf5(
        'fmapRING.hdf5',
        diffusionfn="diffusionRING.pdf",
        fmapfn='fmapRING.pdf')
#
# calculate the map
pyatsoleiloutils.do_SOLEIL_II_fmap(idealring,nproc=64,
        eoffset=0.5e-2,
        fout='fmapIDEALRINGeo.hdf5')

pyatsoleiloutils.do_SOLEIL_II_fmap(ring,nproc=64,
        eoffset=0.5e-2,
        fout='fmapRINGeo.hdf5')

# plot the maps
pyatsoleiloutils.plot_SOLEIL_II_fmaphdf5(
        'fmapIDEALRINGeo.hdf5',
        diffusionfn="diffusionIDEALRINGeo.pdf",
        fmapfn='fmapIDEALRINGeo.pdf')
pyatsoleiloutils.plot_SOLEIL_II_fmaphdf5(
        'fmapRINGeo.hdf5',
        diffusionfn="diffusionRINGeo.pdf",
        fmapfn='fmapRINGeo.pdf')

