# this file calculates the effect of a B9 component in the 412 sextupoles.
# orblancog
# 2023jun13

import pyatsoleiloutils

list_names = ['IDEALRING','RING']
# read the SC structure with the IDEALRING and the one with multipoles
idealring,ring=pyatsoleiloutils.outils4SC.loadSCmatfile('SCmult.mat')
list_rg = [idealring,ring]
rgdict = {list_names[i]: list_rg[i] for i in range(len(list_names))}

lttous_summ = []
for nam in list_names:
    rg = rgdict[nam]
    pyatsoleiloutils.getmomaphdf5_SOLEIL_II(rg, foutn='momap'+nam+'.hdf5')
    lttoui = pyatsoleiloutils.gettouscheklt_SOLEIL_II(rg, finname='momap'+nam+'.hdf5')
    ltous_summ.append(lttoui)
    print(f'Touschek lifetime is {lttoui/3600.:.3f} h ')

print(f'Touschek summary')
print(f'  IDEALRING {ltous_summ[0]/3600.:.3f} h')
print(f'  RING      {ltous_summ[1]/3600.:.3f} h')
print(f'  ratio     {ltous_summ[0]/ltous_summ[1]:.3f}')

