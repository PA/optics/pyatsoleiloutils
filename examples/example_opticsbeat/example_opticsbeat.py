"""
This file contains an example to calculate the optics beat of a ring
from the ideal model
"""
# orblancog 2023may01

import at
import pyatsoleiloutils
from pyatsoleiloutils import opticsbeat

# load rings
ring0 = at.load_mat('file.mat')
# some other ring
ring1 = ring0

ring0.enable_6d()
ring1.enable_6d()
bb, aa, dd, s_coor, s_inds = opticsbeat(ring0,ring1)

# EOF
