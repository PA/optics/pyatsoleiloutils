# orblancog
# generates the frequency map data from the file.mat lattice
# 2023jan16 tracking is parallel, NAFF analysis is serial
# 2022jun07 serial version

import at
from   at.load import load_mat
from   at.load import load_m
from   at.physics import linopt
import numpy as np
from   matplotlib import pyplot as plti

# my functions
import sys
sys.path.append('/home/sources/physmach/blanco-garcia/codes/pyatsoleiloutils/lib')

### read a lattice
fin_mat = 'file.mat';
input_ring = load_mat(fin_mat);
lat_sym = 1;
print(f'Lattice symmetry =\t{lat_sym}');
ring = input_ring * lat_sym;

# check the lattice
[_, beamdata, _]=at.get_optics(ring,get_chrom=True)
print(f'filename         =\t{fin_mat}')
print(f'tune             =\t{beamdata.tune}')
print(f'chromaticity     =\t{beamdata.chromaticity}')

# do fmap analysis
nudiffarray, tunearray = fmap_parallel_track(ring)

# plot diffusion
plt.figure()
marker_size=2;
plt.scatter(nudiffarray[:,0],nudiffarray[:,1],marker_size,c=nudiffarray[:,2])
cbar=plt.colorbar();
cbar.set_label(r'$\log_{10}(|\Delta\nu_x^2 + \Delta\nu_y^2|^{1/2})$')
plt.title("Diffusion Map, SOLEIL II")
plt.xlabel("x (mm)")
plt.ylabel("y (mm)")
plt.xlim(-9,9)
plt.ylim(-3,3)
ax = plt.gca()
ax.set_aspect(1.0)
plt.savefig('diffusion.pdf')

# plot frequency map
plt.figure()
marker_size=2;
plt.scatter(54+tunearray[:,0],18+tunearray[:,1],marker_size,c=tunearray[:,2])
cbar=plt.colorbar();
cbar.set_label(r'$\log_{10}(|\Delta\nu_x^2 + \Delta\nu_y^2|^{1/2})$')
plt.title("Frequency Map, SOLEIL II",fontsize=18)
plt.xlabel(r'$\nu_x$',fontsize=14)
plt.ylabel(r'$\nu_y$',fontsize=14)
plt.xlim(54.12,54.22)
plt.ylim(18.22,18.32)
plt.xticks(fontsize=12)
plt.yticks(fontsize=12)
plt.savefig('fmap.pdf')

