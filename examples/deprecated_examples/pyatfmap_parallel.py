# orblancog
# generates the frequency map data from the file.mat lattice
# 2023jan16 tracking is parallel, NAFF analysis is serial
# 2022jun07 serial version

import at
from at.load import load_mat
from at.load import load_m
from at.physics import linopt
import numpy as np
from matplotlib import pyplot as plt
import PyNAFF
from at.tracking import patpass

fin_mat = 'file.mat';



input_ring = load_mat(fin_mat);
lat_sym = 1;
print(f'Lattice symmetry =\t{lat_sym}');
ring = input_ring * lat_sym;

[_, beamdata, _]=at.get_optics(ring,get_chrom=True)
print(f'filename         =\t{fin_mat}')
print(f'tune             =\t{beamdata.tune}')
print(f'chromaticity     =\t{beamdata.chromaticity}')


tns = 512;  # value of turns for the first and second frequency analysis
nturns = 2*tns;
xscale = 1e-3;
yscale = 1e-3;

nudiffarray=np.empty([])
tunearray=np.empty([])

## test
#ymax = 0.5 #mm
#ymin = -1*ymax
#ystep=0.05
#xmax = 0.5 #mm
#xmin = -1*xmax
#xstep=0.05

ymax = 3 #mm
ymin = -1*ymax
ystep=0.05
xmax = 10 #mm
xmin = -1*xmax
xstep=0.05

# get the intervals
ixarray    = np.arange(xmin, xmax+1e-6, xstep)
lenixarray = len(ixarray);
iyarray    = np.arange(ymin, ymax+1e-6, ystep)
leniyarray = len(iyarray);


print("Start tracking and NAFF analysis")

at.DConstant.patpass_poolsize = 30;
print(f' POOL size : {at.DConstant.patpass_poolsize}')

for iy,iy_index in zip(iyarray, range(leniyarray)):
      print(f'Tracked particles {abs(-100.0*iy_index/leniyarray):.1f} %, with a cpu POOL size of {at.DConstant.patpass_poolsize}');
      print("y =",iy)
      #z01 = np.array([ix*xscale+1e-9, 0,  0, 0,  0, 0])
      # add 1 nm to tracking to avoid zeros in array for the ideal lattice
      z0 = np.zeros((6,lenixarray))
      z0[0,:] = xscale*ixarray + 1e-9 ;
      z0[2,:] = yscale*iy      + 1e-9 ;
      zOUT = patpass(ring, z0, nturns);

      for ix_index in range(lenixarray): # cycle over the track results
          # check if nan in arrays
          array_sum = np.sum(zOUT[:,ix_index,0]);
          array_has_nan = np.isnan(array_sum)
          if array_has_nan:
              print("array has nan")
              continue

      # plt.plot(z1[0, 0, 0, :],z1[1, 0 , 0, :],'.');
      # plt.savefig('fig.pdf');
      # remove mean values

          # get one valid particle
          z1 = zOUT[:,ix_index,0];

          # get the first turn in x
          xfirst     = z1[0, 0:tns];
          xfirst     = xfirst - np.mean(xfirst);
          pxfirst    = z1[1, 0:tns];
          pxfirst    = pxfirst - np.mean(pxfirst);
          xfirstpart = xfirst + 1j*pxfirst;
          # get the last turns in x
          xlast      = z1[0, tns:2*tns];
          xlast      = xlast - np.mean(xlast);
          pxlast     = z1[1, tns:2*tns];
          pxlast     = pxlast - np.mean(pxlast);
          xlastpart  = xlast + 1j*pxlast;

          # get the first turn in y
          yfirst     = z1[2, 0:tns];
          yfirst     = yfirst - np.mean(yfirst);
          pyfirst    = z1[3, 0:tns];
          pyfirst    = pyfirst - np.mean(pyfirst);
          yfirstpart = yfirst + 1j*pyfirst;
          # get the last turns in y
          ylast      = z1[2, tns:2*tns];
          ylast      = ylast - np.mean(ylast);
          pylast     = z1[3, tns:2*tns];
          pylast     = pylast - np.mean(pylast);
          ylastpart  = ylast + 1j*pylast;

  # debug
  #plt.plot(xfirst,pxfirst)
  #plt.savefig('fig.pdf')
  #break

          # calc frequency from array,
          # jump the cycly is no frequency is found
          print("start naff")
          xfreqfirst = PyNAFF.naff(xfirstpart,tns,1,0,False)
          if len(xfreqfirst) == 0: print("  No frequency"); continue;
          print("H freq. first part =\t",xfreqfirst[0][1])
          xfreqlast  = PyNAFF.naff(xlastpart,tns,1,0,False)
          if len(xfreqlast) == 0: print("  No frequency"); continue;
          print("H freq. last part =\t",xfreqlast[0][1])
          yfreqfirst = PyNAFF.naff(yfirstpart,tns,1,0,False)
          if len(yfreqfirst) == 0: print("  No frequency"); continue;
          print("V freq. first part =\t",yfreqfirst[0][1])
          yfreqlast  = PyNAFF.naff(ylastpart,tns,1,0,False)
          if len(yfreqlast) == 0: print("  No frequency"); continue;
          print("V freq. last part =\t",yfreqlast[0][1])

          # metric
          xdiff=xfreqlast[0][1]-xfreqfirst[0][1];
          ydiff=yfreqlast[0][1]-yfreqfirst[0][1];
          nudiff=np.log10(np.sqrt(xdiff*xdiff + ydiff*ydiff))
          # min max diff
          if  nudiff >  -2:
              nudiff =  -2;
          if  nudiff < -10:
              nudiff = -10;
          # save diff
          nudiffarray=np.append(nudiffarray,[ixarray[ix_index],iy,nudiff])
          tunearray=np.append(tunearray,[xfreqfirst[0][1], yfreqfirst[0][1], nudiff])

# first element is garbage
nudiffarray = np.delete(nudiffarray,0);
tunearray   = np.delete(tunearray,0);
## reshape for plots and files
nudiffarray = nudiffarray.reshape(-1,3)
tunearray   = tunearray.reshape(-1,3)

# plot diffusion
plt.figure()
marker_size=2;
plt.scatter(nudiffarray[:,0],nudiffarray[:,1],marker_size,c=nudiffarray[:,2])
cbar=plt.colorbar();
cbar.set_label(r'$\log_{10}(|\Delta\nu_x^2 + \Delta\nu_y^2|^2)$')
plt.title("Diffusion Map, SOLEIL-U")
plt.xlabel("x (mm)")
plt.ylabel("y (mm)")
plt.xlim(-9,9)
plt.ylim(-3,3)
ax = plt.gca()
ax.set_aspect(1.0)
plt.savefig('diffusion.pdf')

# plot frequency map
plt.figure()
marker_size=2;
plt.scatter(54+tunearray[:,0],18+tunearray[:,1],marker_size,c=tunearray[:,2])
cbar=plt.colorbar();
cbar.set_label(r'$\log_{10}(|\Delta\nu_x^2 + \Delta\nu_y^2|^2)$')
plt.title("Frequency Map, SOLEIL-U")
plt.xlabel(r'$\nu_x (mm)$')
plt.ylabel(r'$\nu_y (mm)$')
plt.xlim(54.12,54.22)
plt.ylim(18.22,18.32)
plt.savefig('fmap.pdf')

# save in file
diff_file = open("diffusionmap.txt", "w")
for row in nudiffarray:
    np.savetxt(diff_file, row)
diff_file.close()
tune_file = open("fmap.txt", "w")
for row in tunearray:
    np.savetxt(tune_file, row)
tune_file.close()

exit()

