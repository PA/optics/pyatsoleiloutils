### This is an example to calculate the momentum aperture of a lattice
# orblancog 2023may01 adding pyatsoleiloutils V0.0.3
# orblancog 2023mar01 first release

import at
from at.load import load_m
import numpy

from pyatsoleiloutils import savemomaphdf5

#fin_mat = 'file.mat';
#input_ring = load_mat(fin_mat);
fin_m = 'file.m';
input_ring = load_m(fin_m);
finname = fin_m;

lat_sym = 1;
print(f'Lattice symmetry =\t{lat_sym}');
ring = input_ring * lat_sym;

[_, beamdata, _]=at.get_optics(ring,get_chrom=True)
print(f'filename         =\t{finname}')
print(f'tune             =\t{beamdata.tune}')
print(f'chromaticity     =\t{beamdata.chromaticity}')

lat_sym = 1;
print(f'Lattice symmetry =\t{lat_sym}');
ring = input_ring * lat_sym;

[_, beamdata, _]=at.get_optics(ring,get_chrom=True)
print(f'filename         =\t{finname}')
print(f'tune             =\t{beamdata.tune}')
print(f'chromaticity     =\t{beamdata.chromaticity}')

# check the s coordinate along the machine
s_list = at.get_s_pos(ring, range(len(ring)))
print(len(ring))
print(len(s_list))
# choose unique s
sunique_array, sunique_indexes = numpy.unique(s_list, return_index=True)
print(len(sunique_array))

# Forcing a maximum value of cpus
# I have no idea how to confirm the actual used value from patpassi
#ncpu = 40;
#at.DConstant.patpass_poolsize = ncpu;
#print(f' Requested POOL size : {ncpu}')

#first_index=0
#last_index=4
#s_indexes=sunique_indexes[first_index:last_index] # check lattice section
#s_indexes=sunique_indexes # all unique indexes
s_indexes=sunique_indexes[0:5] # quick test on five points
s_array=sunique_array[0:5]
dp_boundary, _, _ = at.get_momentum_acceptance(ring,
        resolution=1e-3,
        amplitude=0.3,
        dp=0,
        nturns=1024,
        refpts=s_indexes,
        use_mp=True,
        verbose=True,
        divider=10
        )
print(len(dp_boundary))
print(dp_boundary)

savemomaphdf5(
        'momap.hdf5',
        dp_boundary,
        s_indexes,
        s_array
        )

