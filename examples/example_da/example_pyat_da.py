# This python script calculates the dynamic aperture using getacceptance
# and saves the output in a hdf5 file.
# orblancog 2023aug15

import at
from at.load import load_mat
import numpy
import datetime
from pyatsoleiloutils import get_da_SOLEIL_II, savedahdf5

fin = 'file.mat';
ring = load_mat(fin);

[_, beamdata, _] = at.get_optics(ring, get_chrom=True)
print(f'filename         =\t{fin}')
print(f"ringname         =\t{ring.attrs['name']}")
print(f'tune             =\t{beamdata.tune}')
print(f'chromaticity     =\t{beamdata.chromaticity}')

nturns=10
da_boundary = get_da_SOLEIL_II(ring,nturns=nturns)

# sets output filename
tnow = datetime.datetime.now()
foname = 'da' + tnow.strftime("%Y%h%d_%Hh%Mm%S") + '.hdf5'

savedahdf5(foname,nturns=nturns,boundary=da_boundary)

#xx = da_boundary[0][:]
#yy = da_boundary[1][:]
#plt.plot(xx, zz)
#plt.show()

# all done

