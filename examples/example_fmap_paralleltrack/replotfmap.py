"""
This file produces plots into a diffusion.pdf and a fmap.pdf file
"""

from pyatsoleiloutils.plot_SOLEIL_fmap import plot_SOLEIL_II_fmaphdf5

plot_SOLEIL_II_fmaphdf5('fmap.hdf5')

