# example to calculate and plot the BOOSTER lattice frequency map
# orblancog
# 2023jul23 add datetime
# 2023jun08


import numpy
import at
import datetime
from pyatsoleiloutils import do_SOLEIL_II_BOOSTER_fmap, \
    plot_SOLEIL_II_BOOSTER_fmaphdf5

# set output file name
strtimenow  = datetime.datetime.now().strftime("%Y%b%d_%Hh%M")
strfout = 'fmapBOOSTER'+strtimenow+'.hdf5'

# load ring
ring = at.load_mat("Booster_Design6_3_NO_p19_p19_chro_1_1.mat")
ring.disable_6d()

# do fmap
do_SOLEIL_II_BOOSTER_fmap(ring,nproc=4,fout=strfout)
plot_SOLEIL_II_BOOSTER_fmaphdf5 (strfout)

