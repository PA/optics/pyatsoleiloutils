#!/bin/bash
#SBATCH --job-name=pyat_fmap_slurm
#SBATCH --time=30:00:00
#SBATCH --partition=sumo
#SBATCH --qos=parallel
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=24

ml purge
ml tools/openmpi/4.1.1
source /nfs/tegile/work/sources/physmach/blanco-garcia/codes/pyenv/venv_pyatgithub_gcc9.2.0python3.8.10openmpi4.1.5/bin/activate

export PYTHONUNBUFFERED=TRUE
python do_fmap_SOLEIL_II.py -f file.m -n $SLURM_CPUS_PER_TASK > $SLURM_JOB_ID.out 2>$SLURM_JOB_ID.err

