# this file reads a ring from file.m
# and calculates the frequency map
# orblancog
# 2023apr26 call function do_SOLEIL_II_fmap
# 2023apr17 initial release

import at
from at import fmap_parallel_track
import argparse
from pyatsoleiloutils.fmap_analysis_SOLEIL import do_SOLEIL_II_fmap

# parse arguments for parallel computation and input file
argParser = argparse.ArgumentParser()
argParser.add_argument("-f", "--filem", help="file.m")
argParser.add_argument("-n", "--ncpu", help="number of cpus")
args = argParser.parse_args()
#print("args=%s" % args)

# read the ring
ring = at.load_m(args.filem);

# check tune and chromaticity
[_, beamdata, _]=at.get_optics(ring,get_chrom=True)
print(f'tune             =\t{beamdata.tune}')
print(f'chromaticity     =\t{beamdata.chromaticity}')

# set to 4D tracking
print('Turning off radiation and cavity')
ring.disable_6d()

# do fmap withd two processors and create a new file fmap.hdf5
do_SOLEIL_II_fmap(ring,
        fout='fmap.hdf5',
        steps=[20,20],
        eoffset=0.005,
        nproc=args.ncpu)

