"""
This file contains custom elements definition in pyat for SOLEIL
"""

# this file creates/sets custum elements compatible with at
# orblancog
# 2023apr26

import at
import numpy
from scipy.constants import c as clight

__all__ = ['atsetrfcavityharmon']

def atsetrfcavityharmon(
        ring,
        insert_harmonrf_index,
        mainrf_h_number=416,
        harmonrf_h_order=4,
        method='flatpotential',
        verbose=False,
        copyPhysAper=True,
        ):

    # verbose check to check flag only once
    verboseprint = print if verbose else lambda *a, **k: None

    verboseprint('enable 6D on the ring')
    ring.enable_6d()

    # Paramaters for the main and harmonic cavity
    # as in matlab atsetcavity_harm (by L. Nadolsky)
    U0 = ring.get_energy_loss()
    MainHarmNumber = mainrf_h_number
    hcHarmonic = harmonrf_h_order
    freq = (clight/ring.circumference)*MainHarmNumber
    mainVoltage = ring.get_rf_voltage()
    m = hcHarmonic; m2 = m**2
    phis = numpy.arcsin(U0/mainVoltage)
    S1 = m2/(m2-1)*U0/mainVoltage
    mainPhi = numpy.arcsin(S1)
    hcPhi = numpy.arctan(numpy.tan(mainPhi)/m)
    S2 = numpy.sin(hcPhi)
    hcVoltage = -mainVoltage/m2*S1/S2
    orb=at.find_orbit(ring)
    bucketdistance = ring.circumference/MainHarmNumber
    cod = at.find_orbit(ring)
    mainTimelag = (mainPhi)/(2*numpy.pi)*bucketdistance+cod[0][5]
    hcTimelag = (hcPhi)/(2*numpy.pi)*bucketdistance/m+cod[0][5]
    # set main cavity
    ring.set_rf_timelag(mainTimelag)

    # add the harmonic cavity
    LRFharm = 0
    VRFharm = hcVoltage
    fRFharm = hcHarmonic*ring.get_rf_frequency()
    hRFharm = hcHarmonic*MainHarmNumber
    bEnergy = ring.energy
    HARMONICCAVITY = at.RFCavity('HARMONICAVITY', 0,
            VRFharm, fRFharm, hRFharm, bEnergy)
    ring.insert(insert_harmonrf_index, HARMONICCAVITY)

    # set timelag
    harmonrf_index = insert_harmonrf_index
    ring.set_rf_timelag(hcTimelag, cavpts=harmonrf_index)

    if copyPhysAper:
        if hasattr(ring[harmonrf_index+1],'EApertures'):
            ring[harmonrf_index].EApertures = \
                    ring[harmonrf_index+1].EApertures
        elif hasattr(ring[harmonrf_index+1],'RApertures'):
            ring[harmonrf_index].RApertures = \
                    ring[harmonrf_index+1].RApertures
        else:
            verboseprint('No apertures have been found')

    return ring

# the end
