"""add functions for SC"""

import scipy
import at
import os

__all__ = ['loadSCmatfile','loadringsfromSC']

def mclean(data):
    if data.dtype.type is numpy.str_:
        # Convert strings in arrays back to strings.
        return str(data[0]) if data.size > 0 else ''
    elif data.size == 1:
        v = data[0, 0]
        if issubclass(v.dtype.type, numpy.void):
            # Object => Return a dict
            return {f: mclean(v[f]) for f in v.dtype.fields}
        else:
            # Return a scalar
            return v
    else:
        # Remove any surplus dimensions in arrays.
        return numpy.squeeze(data)

#cell_array = m[key].flat
#    for index, mat_elem in enumerate(cell_array):
#        elem = mat_elem[0, 0]
#        kwargs = {f: mclean(elem[f]) for f in elem.dtype.fields}
#        yield element_from_dict(kwargs, index=index, check=check, quiet=quiet)


def loadSCmatfile(fname):
  """
  loadSCmatfile(fname)
  This function loads an SC structure saved in a .mat file
  Args:
    fname is thecomplete .mat filename
  Returns:
    idealring, ring
  """
  SCmat = scipy.io.loadmat(fname);
  idealring, ring = loadringsfromSC(SCmat)
  return idealring, ring


def loadringsfromSC(SCstruct):
  """
  loadringsfromSC(SCstruct)
  This function reads idealring and ring from an SC structure.
  Args:
    SC structure
  Returns:
    idealring, ring
  """
  list_of_ringnames = ['IDEALRING','RING']
  list_of_rings = []
  matvars = [varname for varname in SCstruct if not varname.startswith('__')]
  default_key = matvars[0] if (len(matvars) == 1) else 'SC'

  for ri in list_of_ringnames:
    scipy.io.savemat('tmpringfromSC.mat', {ri: SCstruct[default_key][ri][0,0]})
    ring = at.load_mat('tmpringfromSC.mat', mat_key=ri, keep_all=True)
    os.remove('tmpringfromSC.mat')
    list_of_rings.append(ring)

  return list_of_rings[0], list_of_rings[1]
