"""
This file calculates optics parameters for the SOLEIL rings
"""
# orblancog 2023may01 first release

import numpy
import at
from at import linopt6, linopt4


__all__ = ["opticsbeat"]

def opticsbeat(
        ring0,
        ring,
        refpts=None,
        dp=None
        ):
    """
    This function calculates the optics beat for a given pair of optics
    Args:
      ring0  : unperturbed ring
      ring   : perturbed ring
      refpts : reference points (default is all elements)
      dp     : energy offset (default is None)

    Returns:
      dbetaxy/betaxy0 array with shape (refpts,2)
      dalphaxy array with shape (refpts,2)
      detaxy array with shape (refpts,2)
      s_array = list of s coordinates
      s_indexes = list of indexes along the lattice
    """

    # set the reference points
    if refpts is None:
        refpts = ring0.get_uint32_index(at.All, endpoint=False)
    else:
        refpts = ring0.get_uint32_index(refpts)

    if ring0.is_6d and ring.is_6d:
        eldat0s0, rdata0, eldat0refpts = linopt6(ring0, dp=dp, refpts=refpts)
        eldats0, rdata, eldatrefpts = linopt6(ring, dp=dp, refpts=refpts)
    else:
        print(f'Warning, rings are not 6D, moving to 4D')
        eldat0s0, rdata0, eldat0refpts = linopt4(ring0, dp=dp, refpts=refpts)
        eldats0, rdata, eldatrefpts = linopt4(ring, dp=dp, refpts=refpts)

    # dbeta/beta0 (x,y)
    dbetaratio = numpy.divide(
                                (eldatrefpts['beta'] - eldat0refpts['beta']),
                                eldat0refpts['beta']
                            )
    # dalpha (x,y)
    dalpha = eldatrefpts['alpha'] - eldat0refpts['alpha']

    # deta (x,pyx,y,py)
    deta = eldatrefpts['dispersion'] - eldat0refpts['dispersion']

    # s coordinate
    s_list = at.get_s_pos(ring, refpts)
    s_index = refpts

    return dbetaratio, dalpha, deta, s_list, s_index

# EOF
