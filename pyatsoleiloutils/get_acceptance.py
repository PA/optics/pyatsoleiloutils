"""
This file contains the functions to calculate the dynamic aperture
of a ring
"""

# orblancog
# 2023ago13

import at
import numpy

__all__ = ['get_da_SOLEIL_II']

def get_da_SOLEIL_II(ring, **kwargs):
    """
    This functions calculates the dynamic aperture or a ring
    """
    coSTART = at.find_orbit(ring)

    ndims = ['x','y']
    amplitude = kwargs.pop('amplitude',0.01)
    resolution = kwargs.pop('resolution',0.1e-3)
    npoints = numpy.array([ numpy.ceil(amplitude/resolution),
                            numpy.ceil(amplitude/resolution)])
    amplitudes = numpy.array([amplitude,amplitude])
    nturns = kwargs.pop('nturns',512)
    da_boundary_with_get_acceptance, _, _ = at.get_acceptance(
            ring,
            ndims,
            npoints,
            amplitudes,
            nturns=nturns,
            offset=coSTART[0],
            grid_mode=at.GridMode(1),
            use_mp=True,
            verbose=True
            )

    return da_boundary_with_get_acceptance

