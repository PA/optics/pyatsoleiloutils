"""
This file contains functions to read preformated data from
hdf5 files
"""

import h5py
import numpy

__all__ = ['readmomaphdf5']

def readmomaphdf5(
        fname
        ):
    """
    momap_pn, s_indexes, s_array = readmomaphdf5(fname)

    This functions reads a given hdf5 with momentum aperture data
    saved with the function savemomaphdf5.

    Args:
      fname: name of the input hdf5 file
    Returns: (either 3 or 5 elements)
      momap: positive and negative sides of momentum aperture
      s_indexes: indexes where momap was calculated
      s_array: s coordinates of s_indexes
      and
      if the file contains index_first element and ring_length it returns them
    """
    f = h5py.File(fname, 'r')

    long_output = 0
    # set momap input format for get_lifetime
    momap_pn = numpy.array((f['momap']['dppP'][:],
         f['momap']['dppN'][:]))
    momap_pn = numpy.copy(momap_pn.T)
    s_indexes=f['momap']['s_indexes'][:]
    s_array=f['momap']['s_array'][:]
    if 'index_first_element' in f['momap']:
        long_output = 1
        index_first_element = f['momap']['index_first_element'][:]
    else:
        print('Warning: no first element index, assume zero')
        index_first_element =  0
    if 'ring_length' in f['momap']:
        long_output = 1
        ring_length = f['momap']['ring_length'][:]
        if ring_length < 0:
            print('Warning: ring length is negative')
    else:
        print('Warning: no ring length in file, set to -1')
        ring_length = -1
    f.close()

    if long_output == 0:
        return momap_pn, s_indexes, s_array
    else:
        return momap_pn, s_indexes, s_array, index_first_element, ring_length

