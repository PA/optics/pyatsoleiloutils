"""
This file contains functions to perform frequency analysis
on the SOLEIL lattices
"""
# orblancog 2023may01

import numpy
from at import fmap_parallel_track
from .savehdf5file import savefmahdf5

__all__ = ['do_SOLEIL_II_fmap',
        'do_SOLEIL_II_BOOSTER_fmap']

def do_SOLEIL_II_fmap(
        ring,
        nproc,
        fout="fmap.hdf5",
        steps=[200,200],
        eoffset=0,
        verbose=False
        ):
    """
    This function calculates the frequency map with
    predefined parameters for the SOLEIL II lattice.
    It requires,
    - ring  : a valid at ring, typically with 6D disabled
    - nproc : number of processors
    - fout  : the name of a hdf5 output file
                 by default "fmap.hdf5"
    """

    # launch the calculation
    [xy_tune_nuxy_nudiff_array, plosses_array] = \
            fmap_parallel_track(
                    ring,
                    coords=[-8,8,-3,3],
                    steps=steps,
                    scale='linear',
                    add_offset6D=numpy.array([0, 0, 0, 0, eoffset, 0]),
                    lossmap=True,
                    pool_size=int(nproc),
                    verbose=verbose
                    )

    # save output into an hdf5 file
    savefmahdf5(fout,
            xy_tune_nuxy_nudiff_array
            )

def do_SOLEIL_II_BOOSTER_fmap(
        ring,
        nproc,
        fout="fmapBOOSTERII.hdf5",
        steps=[200,200],
        eoffset=0,
        verbose=False
        ):
    """
    This function calculates the frequency map with
    predefined parameters for the SOLEIL II lattice.
    It requires,
    - ring  : a valid at ring, typically with 6D disabled
    - nproc : number of processors
    - fout  : the name of a hdf5 output file
                 by default "fmap.hdf5"
    """

    # launch the calculation
    [xy_tune_nuxy_nudiff_array, plosses_array] = \
            fmap_parallel_track(
                    ring,
                    coords=[-60,60,-50,50],
                    steps=steps,
                    scale='linear',
                    add_offset6D=numpy.array([0, 0, 0, 0, eoffset, 0]),
                    lossmap=True,
                    pool_size=int(nproc),
                    verbose=verbose
                    )

    # save output into an hdf5 file
    savefmahdf5(fout,
            xy_tune_nuxy_nudiff_array
            )

# the end

