"""Plot a lattice synoptic"""


# orblancog

# 2023jan17 Add voffset and fix bug in None parameter for Monitors and correctors
# 2023jan16 Vadim's bug fix on the set_alpha and zorder
# 2022jul13 first release

import numpy
# noinspection PyPackageRequirements
import matplotlib.pyplot as plt
# noinspection PyPackageRequirements
from matplotlib.patches import Polygon, PathPatch
from matplotlib.path import Path
# noinspection PyPackageRequirements
from matplotlib.collections import PatchCollection
from at.lattice import elements as elts
import re

__all__ = ['atplotsynSOLEIL']

# Default properties for element representation
DIPOLE      = dict(label='Dipoles',     facecolor=(1.0, 0.6, 0.6), edgecolor = 'k')
ANTIBEND    = dict(label='Antibends',   facecolor=(0.8, 1.0, 0.8), edgecolor = 'k')
QUADRUPOLE  = dict(label='Quadrupoles', facecolor=(0.6, 0.6, 1.0), edgecolor = 'k')
SEXTUPOLE   = dict(label='Sextupoles',  facecolor=(1.0, 1.0, 0.0), edgecolor = 'k')
OCTUPOLE    = dict(label='Octupoles',   facecolor=(0.0, 0.7, 0.0), edgecolor = 'k')
MULTIPOLE   = dict(label='Multipoles',  facecolor=(0.0, 0.5, 0.0), edgecolor = 'k')
MONITOR     = dict(label='Monitors',    linestyle='None', marker="v", color='k',  markersize=10)
CORRECTOR   = dict(label='Correctors',  linestyle='None', marker="D", color='y',  markersize=10, markeredgecolor = 'k')
CORRECTORH  = dict(label='Correctors',  linestyle='None', marker="D", color='b',  markersize=10, markeredgecolor = 'k')
CORRECTORV  = dict(label='Correctors',  linestyle='None', marker="D", color='g',  markersize=10, markeredgecolor = 'k')
GIRDERSTART = dict(label='Girderstart', linestyle="dashed", color='k', lw = 3, facecolor = "none")
GIRDEREND   = dict(label='Girderend',   linestyle="dashed", color='r', lw = 3, facecolor = "none")


# noinspection PyDefaultArgument

#[docs]
def atplotsynSOLEIL(ring, axes=None, voffset=0, dipole={}, quadrupole={}, sextupole={},
                multipole={}, monitor={}, corrector={}, correctorh={}, correctorv={},
                antibend={}, octupole={}, girderstart={}, girderend={}):
    """Plot a synoptic of a lattice

    PARAMETERS
        ring            Lattice object

    KEYWORDS
        s_range=None    plot range, defaults to the full ring
        axes=None       axes for plotting the synoptic. If None, a new
                        figure will be created. Otherwise, a new axes object
                        sharing the same x-axis as the given one is created.
        dipole={}       Dictionary of properties overloading the default
                        properties. If None, dipoles will not be shown.
        antibend={}     Same definition as for dipole
        quadrupole={}   Same definition as for dipole
        sextupole={}    Same definition as for dipole
        octupole={}     Same definition as for dipole
        multipole={}    Same definition as for dipole
        monitor={}      Same definition as for dipole
        corrector={}    Same definition as for dipole
        correctorH={}   Same definition as for dipole
        correctorV={}   Same definition as for dipole
        girderstart={}  Same definition as for dipole
        girderend={}    Same definition as for dipole

    RETURN
        synopt_axes     Synoptic axes
     """

    class Dipole(Polygon):
        xx = numpy.array([0, 0, 1, 1], dtype=float)
        yy = numpy.array([0, 1, 1, 0], dtype=float)

        def __init__(self, s, length, **kwargs):
            xy = numpy.stack((self.xx * length + s, self.yy + voffset), axis=1)
            super(Dipole, self).__init__(xy, closed=False, **kwargs)

    class Antibend(Polygon):
        xx = numpy.array([0, 0, 1, 1], dtype=float)
        yy = numpy.array([0, 1, 1, 0], dtype=float)

        def __init__(self, s, length, **kwargs):
            xy = numpy.stack((self.xx * length + s, self.yy + voffset), axis=1)
            super(Antibend, self).__init__(xy, closed=False, **kwargs)

    class Quadrupole(Polygon):
        xx = numpy.array([0, 0, 0.5, 1, 1])
        yy = {True: numpy.array([0, 1, 1.4, 1, 0]),
              False: numpy.array([0, 1, 0.6, 1, 0])}

        def __init__(self, s, length, foc, **kwargs):
            xy = numpy.stack((self.xx * length + s, self.yy[foc] + voffset), axis=1)
            super(Quadrupole, self).__init__(xy, closed=False, **kwargs)

    class Sextupole(Polygon):
        xx = numpy.array([0, 0, 0.33, 0.66, 1, 1])
        yy = {True: numpy.array([0, 0.8, 1, 1, 0.8, 0]),
              False: numpy.array([0, 0.8, 0.6, 0.6, 0.8, 0])}

        def __init__(self, s, length, foc, **kwargs):
            xy = numpy.stack((self.xx * length + s, self.yy[foc] + voffset), axis=1)
            super(Sextupole, self).__init__(xy, closed=False, **kwargs)

    class Octupole(Polygon):
        xx = numpy.array([0, 0, 1, 1], dtype=float)
        yy = numpy.array([0, 0.8, 0.8, 0])

        def __init__(self, s, length, **kwargs):
            xy = numpy.stack((self.xx * length + s, self.yy + voffset), axis=1)
            super(Octupole, self).__init__(xy, closed=False, **kwargs)

    class Multipole(Polygon):
        xx = numpy.array([0, 0, 1, 1], dtype=float)
        yy = numpy.array([0, 0.8, 0.8, 0])

        def __init__(self, s, length, **kwargs):
            xy = numpy.stack((self.xx * length + s, self.yy + voffset), axis=1)
            super(Multipole, self).__init__(xy, closed=False, **kwargs)

    class Monitor(Polygon):
        xx = numpy.array([0.0, 1.0])
        yy = numpy.array([0.0, 10.0])

        def __init__(self, s, **kwargs):
            xy = numpy.stack((self.xx + s, self.yy + voffset), axis=1)
            super(Monitor, self).__init__(xy, closed=False, **kwargs)

    class Corrector(Polygon):
        xx = numpy.array([0.0, 0.0])
        yy = numpy.array([0.0, 1.2])

        def __init__(self, s, **kwargs):
            xy = numpy.stack((self.xx + s, self.yy + voffset), axis=1)
            super(Corrector, self).__init__(xy, closed=False, **kwargs)

    class CorrectorH(Polygon):
        xx = numpy.array([0.0, 0.0])
        yy = numpy.array([0.0, 1.2])

        def __init__(self, s, **kwargs):
            xy = numpy.stack((self.xx + s, self.yy + voffset), axis=1)
            super(CorrectorH, self).__init__(xy, closed=False, **kwargs)

    class CorrectorV(Polygon):
        xx = numpy.array([0.0, 0.0])
        yy = numpy.array([0.0, 1.2])

        def __init__(self, s, **kwargs):
            xy = numpy.stack((self.xx + s, self.yy + voffset), axis=1)
            super(CorrectorV, self).__init__(xy, closed=False, **kwargs)

    class Girderstart(Path):

        def __init__(self, s, **kwargs):
            verts = [(0.0 + s,  0.0 + voffset), (0.0 + s, -1.0 + voffset)]
            codes = [Path.MOVETO, Path.LINETO]
            super(Girderstart, self).__init__(verts, codes,**kwargs)

    class Girderend(Path):

        def __init__(self, s, **kwargs):
            verts = [(0.0 + s,  0.0 + voffset), (0.0 + s, -1.0 + voffset)]
            codes = [Path.MOVETO, Path.LINETO]
            super(Girderend, self).__init__(verts, codes,**kwargs)

    def ismultipole(elem):
        return isinstance(elem, elts.Multipole) and not isinstance(elem, (
            elts.Dipole, elts.Quadrupole, elts.Sextupole))

    if axes is None:
        fig = plt.figure()
        axsyn = fig.add_subplot(111, xlim=ring.s_range)
    else:
        axsyn = axes.twinx()
    axsyn.set_axis_off()         # Set axis invisible
    axsyn.set_ylim((-2.0, 20.0)) # Initial scaling of elements
    axsyn.set_zorder(0.2)        # Put synoptic in the front
    axsyn.patch.set_alpha(0.01)  # Set background almost transparent

    s_pos = ring.get_s_pos(range(len(ring)))

    if dipole is not None:
        props = DIPOLE.copy()
        props.update(dipole)
        dipoles = PatchCollection(
            (Dipole(s, el.Length) for s, el in zip(s_pos, ring)
             if ( isinstance(el, elts.Dipole) and bool(re.match(r'BD|CHIC',el.FamName)))), **props)
        axsyn.add_collection(dipoles)

    if antibend is not None:
        props = ANTIBEND.copy()
        props.update(antibend)
        antibends = PatchCollection(
            (Antibend(s, el.Length) for s, el in zip(s_pos, ring)
             if ( isinstance(el, elts.Dipole) and bool(re.match(r'BQ|CHIC',el.FamName)))), **props)
        axsyn.add_collection(antibends)

    if quadrupole is not None:
        props = QUADRUPOLE.copy()
        props.update(quadrupole)
        quadrupoles = PatchCollection(
            (Quadrupole(s, el.Length, el.PolynomB[1] >= 0.0)
             for s, el in zip(s_pos, ring)
             if isinstance(el, elts.Quadrupole)), **props)
        axsyn.add_collection(quadrupoles)

    if sextupole is not None:
        props = SEXTUPOLE.copy()
        props.update(sextupole)
        sextupoles = PatchCollection(
            (Sextupole(s, el.Length, el.PolynomB[2] >= 0.0)
             for s, el in zip(s_pos, ring)
             if isinstance(el, elts.Sextupole)), **props)
        axsyn.add_collection(sextupoles)

    if octupole is not None:
        props = OCTUPOLE.copy()
        props.update(octupole)
        octupoles = PatchCollection(
            (Octupole(s, el.Length) for s, el in zip(s_pos, ring)
             if (ismultipole(el) and bool(re.match(r'Octupole',el.Class)))), **props)
        axsyn.add_collection(octupoles)

    if multipole is not None:
        props = MULTIPOLE.copy()
        props.update(multipole)
        multipoles = PatchCollection(
            (Multipole(s, el.Length) for s, el in zip(s_pos, ring)
             if ismultipole(el)), **props)
        axsyn.add_collection(multipoles)

    if monitor is not None:
        props = MONITOR.copy()
        props.update(monitor)
        s = s_pos[[isinstance(el, elts.Monitor) for el in ring]]
        y = numpy.zeros(s.shape) + voffset
        # noinspection PyUnusedLocal
        monitors = axsyn.plot(s, y, **props)

    if corrector is not None:
        props = CORRECTOR.copy()
        props.update(corrector)
        s = s_pos[[isinstance(el, elts.Corrector) for el in ring]]
        y = numpy.zeros(s.shape) + voffset
        # noinspection PyUnusedLocal
        correctors = axsyn.plot(s, y, **props)

    if correctorh is not None:
        props = CORRECTORH.copy()
        props.update(corrector)
        s = s_pos[[(isinstance(el, elts.Corrector) and bool(re.match(r'HCOR',el.FamName))) for el in ring]]
        y = numpy.zeros(s.shape) + voffset
        # noinspection PyUnusedLocal
        correctors = axsyn.plot(s, y, **props)

    if correctorv is not None:
        props = CORRECTORV.copy()
        props.update(corrector)
        s = s_pos[[(isinstance(el, elts.Corrector) and bool(re.match(r'VCOR',el.FamName))) for el in ring]]
        y = numpy.zeros(s.shape) + voffset
        # noinspection PyUnusedLocal
        correctors = axsyn.plot(s, y, **props)

    if girderstart is not None:
        props = GIRDERSTART.copy()
        props.update(girderstart)
        girders_start = PatchCollection(
                (PathPatch(Girderstart(s)) for s, el in zip(s_pos, ring)
                if ( isinstance(el, elts.Marker) and bool(re.match(r'GIRDERSTART',el.FamName)))), **props)
        axsyn.add_collection(girders_start)

    if girderend is not None:
        props = GIRDEREND.copy()
        props.update(girderend)
        girders_end = PatchCollection(
                (PathPatch(Girderend(s)) for s, el in zip(s_pos, ring)
                if ( isinstance(el, elts.Marker) and bool(re.match(r'GIRDEREND',el.FamName)))), **props)
        axsyn.add_collection(girders_end)

    return axsyn

