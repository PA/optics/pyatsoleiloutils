"""
This file contains functions to read preformated data from
mat files
"""

import scipy
import numpy

__all__ = ['readmomapmat']

def readmomapmat(
        fname
        ):
    """
    momap_pn, s_indexes, s_array, index_first_element = readmomapmat(fname)

    This functions reads a given .mat with momentum aperture data

    Args:
      fname: name of the input .mat file
    Returns:
      momap: positive and negative sides of momentum aperture
      s_indexes: indexes where momap was calculated
      s_array: s coordinates of s_indexes
      index_first_element : returns the first element index
                              0 (in python), 1 (in matlab)
    """
    f = scipy.io.loadmat(fname)

    # set momap input format for get_lifetime
    momap_pn = numpy.array((f['momap']['dppP'][0,0][:,0],
         f['momap']['dppN'][0,0][:,0]))
    momap_pn = numpy.copy(momap_pn.T)
    s_indexes=f['momap']['s_indexes'][0,0][:,0]
    s_array=f['momap']['s_array'][0,0][:,0]
    nturns=f['momap']['nturns'][0,0][:,0]
    index_first_element = f['momap']['index_first_element'][0,0][:,0]

    return momap_pn, s_indexes, s_array, index_first_element

