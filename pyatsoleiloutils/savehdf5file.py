"""
This file contains functions to save data into hdf5 files
"""
# orblancog
# 2023may01

import h5py
import numpy

__all__ = ['savemomaphdf5','savefmahdf5','savedahdf5']

def savemomaphdf5(
        fname,
        dp_boundary,
        s_indexes,
        s_array,
        **kwargs
        ):
    """
    This function saves a hdf5 file with the output of a momentum aperture
    calculation in pyat and additional data for a plot

    Args:
      fname: name of the output hdf5 filename
      dp_boundary: output from at.get_momentum_acceptance
      s_indexes: indexes where momentum aperture was calculated
      s_array: s coordinate for the s_indexes
      **kwargs
    """
    index_first_element=kwargs.pop('index_first_element',0)
    ring_length=kwargs.pop('ring_length',-1)

    # ff = h5py.File(fname, 'w')
    ff = h5py.File(fname, 'a')
    groupmomap = ff.create_group('momap')
    groupmomap.create_dataset("dppN", data=dp_boundary[:,0])
    groupmomap.create_dataset("dppP", data=dp_boundary[:,1])
    groupmomap.create_dataset("s_indexes", data=s_indexes)
    groupmomap.create_dataset("s_array", data=s_array)
    groupmomap.create_dataset("index_first_element", data=numpy.array([int(index_first_element)]))
    groupmomap.create_dataset("ring_length", data=numpy.array([int(ring_length)]))
    # groupmomap.create_dataset("ring", data=ring) # it does not work
    ff.close()


def savefmahdf5(
        fname,
        xy_tune_nuxy_nudiff_array
        ):
    """
    This function saves into an hdf5 file the fmap result
    from at
    """

    ff = h5py.File(fname, 'w') 
    fma_group=ff.create_group('fma')
    fma_group.create_dataset('x',data=xy_tune_nuxy_nudiff_array[:,0])
    fma_group.create_dataset('y',data=xy_tune_nuxy_nudiff_array[:,1])
    fma_group.create_dataset('nux',data=xy_tune_nuxy_nudiff_array[:,2])
    fma_group.create_dataset('nuy',data=xy_tune_nuxy_nudiff_array[:,3])
    fma_group.create_dataset('dnux',data=xy_tune_nuxy_nudiff_array[:,4])
    fma_group.create_dataset('dnuy',data=xy_tune_nuxy_nudiff_array[:,5])
    fma_group.create_dataset('dnu',data=xy_tune_nuxy_nudiff_array[:,6])
    ff.close()


def savedahdf5(fname, **kwargs):
    """
    This function saves the contour of dynamic aperture in an hdf5 file
    """
    ff = h5py.File(fname, 'w')
    da_group = ff.create_group('da')
    da_group.create_dataset('nturns', data=numpy.array([kwargs.pop('nturns')]))
    #da_group.create_dataset('ndims', data=numpy.array(kwargs.pop('ndims')))
    #da_group.create_dataset('amplitudes', data=numpy.array([kwargs.pop('amplitudes')]))
    #da_group.create_dataset('resolution', data=numpy.array([kwargs.pop('resolution')]))
    boundary = kwargs.pop('boundary')
    boundary_group = da_group.create_group('boundary')
    boundary_group.create_dataset('x', data=boundary[0,:])
    boundary_group.create_dataset('y', data=boundary[1,:])
    ff.close()


# the end
