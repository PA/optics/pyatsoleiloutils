"""
Plot frequency map results from pyat fmap analysis
"""

# orblancog
# 2023apr23 moving functions to packages
# 2023jan16 plot fmap with fixed axis

import numpy
import h5py
from matplotlib import pyplot as plt

__all__ = ['plot_SOLEIL_II_fmaphdf5',
        'plot_SOLEIL_II_BOOSTER_fmaphdf5',
        'plot_SOLEIL_II_fmap',
        'plot_SOLEIL_II_BOOSTER_fmap'
        ]


def read_fmaphdf5(fmapfn):

    ff = h5py.File(fmapfn,'r')
    # check dimension
    npoints=numpy.shape(ff['fma']['x'])
    # create output array
    fmap_array=numpy.empty((npoints[0], 7))
    # fill the array
    fmap_array[:,0]=ff['fma']['x']
    fmap_array[:,1]=ff['fma']['y']
    fmap_array[:,2]=ff['fma']['nux']
    fmap_array[:,3]=ff['fma']['nuy']
    fmap_array[:,4]=ff['fma']['dnux']
    fmap_array[:,5]=ff['fma']['dnuy']
    fmap_array[:,6]=ff['fma']['dnu']
    ff.close()

    return fmap_array


def plot_SOLEIL_II_fmaphdf5(fmaphdf5,
        diffusionfn='diffussion.pdf',
        fmapfn='fmap.pdf',
        addxlims=[-9,9],
        addylims=[-3,3],
        integertune=[54,18],
        nuxzoom=[0.12,0.22],
        nuyzoom=[0.22,0.32]
        ):
    """
    Plot the SOLEIL II map from a hdf5 file into a pdf/png file.
    """
    fmap_array = read_fmaphdf5(fmaphdf5)
    pltfmap(fmap_array,
            addtitle='SOLEIL II',
            addxlims=addxlims,
            addylims=addylims,
            diffusionfilename=diffusionfn,
            fmapfilename=fmapfn,
            addintegertune=integertune,
            nuxzoom=nuxzoom,
            nuyzoom=nuyzoom
            )


def plot_SOLEIL_II_BOOSTER_fmaphdf5(fmaphdf5,
        diffusionfn='diffussionBOOSTER.pdf',
        fmapfn='fmapBOOSTER.pdf',
        addxlims=[-60,60],
        addylims=[-50,50],
        integertune=[0,0],
        nuxzoom=[0.0,0.5],
        nuyzoom=[0.0,0.5]
        ):
    """
    Plot the SOLEIL II BOOSTER map from a hdf5 file into a pdf/png file.
    """
    fmap_array = read_fmaphdf5(fmaphdf5)
    pltfmap(fmap_array,
            addtitle='SOLEIL II BOOSTER',
            addxlims=addxlims,
            addylims=addylims,
            diffusionfilename=diffusionfn,
            fmapfilename=fmapfn,
            addintegertune=integertune,
            nuxzoom=nuxzoom,
            nuyzoom=nuyzoom
            )


def plot_SOLEIL_II_fmap(fmap_array):
    pltfmap(fmap_array)

def plot_SOLEIL_II_BOOSTER_fmap(fmap_array):
    pltfmap(fmap_array)

def pltfmap(fmap_array,
        addtitle='SOLEIL II',
        addxlims=[-9,9],
        addylims=[-3,3],
        diffusionfilename='diffusion.pdf',
        addintegertune=[0,0],
        nuxzoom=[0.12,0.22],
        nuyzoom=[0.22,0.32],
        fmapfilename='fmap.pdf'
        ):
    # plot diffusion
    plt.figure()
    marker_size=2;
    plt.scatter(fmap_array[:,0], fmap_array[:,1], marker_size,
            c=fmap_array[:,6], vmin=-10, vmax=-2)
    # plt.scatter(fmap_array['fma']['x'][:],
    #       fmap_array['fma']['y'][:], marker_size,
    #       c=fmap_array['fma']['dnu'][:], vmin=-10, vmax=-2)
    cbar=plt.colorbar(shrink=0.4);
    cbar.set_label(r'$\log_{10}(|\Delta\nu_x^2 + \Delta\nu_y^2|^{1/2}/turn)$')
    plt.title(f'Diffusion Map, {addtitle}')
    plt.xlabel("x (mm)")
    plt.ylabel("y (mm)")
    plt.xlim(addxlims[0],addxlims[1])
    plt.ylim(addylims[0],addylims[1])
    ax = plt.gca()
    ax.set_aspect(1.0)
    plt.savefig(f'{diffusionfilename}')

    # plot frequency map
    plt.figure()
    marker_size=2;
    plt.scatter(addintegertune[0]+fmap_array[:,2],
            addintegertune[1]+fmap_array[:,3], marker_size,
            c=fmap_array[:,6], vmin=-10, vmax=-2)
    #plt.scatter(54+fmap_array['fma']['nux'][:],
    #18+fmap_array['fma']['nuy'][:], marker_size,
    #c=fmap_array['fma']['dnu'][:], vmin=-10, vmax=-2)
    cbar=plt.colorbar();
    cbar.set_label(r'$\log_{10}(|\Delta\nu_x^2 + \Delta\nu_y^2|^{1/2}/turn)$')
    plt.title(f'Frequency Map, {addtitle}',fontsize=18)
    plt.xlabel(r'$\nu_x$',fontsize=14)
    plt.ylabel(r'$\nu_y$',fontsize=14)
    plt.xlim(addintegertune[0]+nuxzoom[0],
            addintegertune[0]+nuxzoom[1])
    plt.ylim(addintegertune[1]+nuyzoom[0],
            addintegertune[1]+nuyzoom[1])
    plt.xticks(fontsize=12)
    plt.yticks(fontsize=12)
    plt.savefig(f'{fmapfilename}')

#the end
