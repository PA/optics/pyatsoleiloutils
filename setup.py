import setuptools
setuptools.setup()


#with open('README.md','r', encoding="utf-8") as fh:
#    long_description = fh.read()
#
#setuptools.setup(
#        name='pyatsoleiloutils',
#        version='0.0.4',
#        author='orblancog',
#        author_email='oscar-roberto.blanco-garcia@synchrotron-soleil.fr',
#        description="pyat complement library for SOLEIL II",
#        long_description=long_description,
#        long_description_content_type="text/markdown",
#        url="https://gitlab.synchrotron-soleil.fr",
#        packages=['pyatsoleiloutils'],
#        license='GPL-3.0-or-later',
#        classifiers=[
#            "Programming Language :: Python 3",
#            "Licence :: GNU v3",
#            "Operating System :: not defined"
#            ],
#        )
#
